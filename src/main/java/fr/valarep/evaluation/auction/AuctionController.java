package fr.valarep.evaluation.auction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/auction")
public class AuctionController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

   @Autowired
   AuctionRepository auctionRepository;

   Auction auction;

   @PostMapping
   public void CreateEnchere (@RequestBody Auction auction){
        Date now = new Date();
        if (auction.getStartDate().after(now) || auction.getEndDate().after(now)){
            throw new IllegalArgumentException("Start date and end date must be after today");
        }
        if (auction.getStartDate().after(auction.getEndDate())){
            throw new IllegalArgumentException("Start date must be before end date");
        }
        if (auction.getStartPrice() <= 0){
            throw new IllegalArgumentException("Start price must be over than 0");
        }
        if (auction.getProduct().isEmpty()){
            throw new IllegalArgumentException("Product musn't be empty");
        }
       auctionRepository.save(auction);


   }

    @GetMapping
    public List<Auction> getAll(){return auctionRepository.findAll();}

    @PutMapping
    public void updateEnchere(@RequestBody Auction auction){
        auctionRepository.save(auction);
        logger.info("put OK");
    }
}



